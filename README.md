Transform a local mail directory into content files, to be processed with a [static site generator](https://www.staticgen.com/). This tool is useful for several scenarios, including:

* Posting by mail (similar to [Wordpress’ ability](https://en.support.wordpress.com/post-by-email/)).
* Creating a web archive of a mailing list. You can see it in use [here](http://ginateva.net/); all of the posts are automatically created using mail2staticsite from mailing list messages.

Unlike [JekyllMail](https://github.com/masukomi/JekyllMail), it is completely local and static. The workflow is as following: **mail** → mail2staticsite → **plaintext+attachments** → static site generator → **website**.

mail2staticsite extracts all the data it needs from the mail message:

input          | output
-------------- | ------
subject        | title\*
body           | content\*
date           | date
attachments    | added at the end of the post

\* can be processed using Python. By default it removes common prefixes from the subject and if the result is an empty string, it substitutes it with EMPTY_SUBJECT_TITLE.



# Dependencies

* Python 3.3+

The mail directory can be populated and managed by any program. I recommend using [OfflineIMAP](https://en.wikipedia.org/wiki/OfflineIMAP) for synchronization and [Mutt](https://en.wikipedia.org/wiki/Mutt_(email_client)) for management, but as long as the input is a valid [Maildir](https://en.wikipedia.org/wiki/Maildir) format, everything should be fine.



# Static site generator support

Currently only [Pelican](http://getpelican.com/) is supported. Adding support for additional static site generators should be really easy. Feel free to create a new issue if you want me to add support for a certain one, or to send a merge request if you’ve done so yourself.



# Configuration

Configuration is done using Python configuration file:

key                   | explanation
--------------------- | -----------
MAIL_DIR              | mail directory to be processed
CONTENT_DIR           | directory for the plain text files output
MEDIA_DIR             | directory for the attachments
STATIC_SITE_GENERATOR | currently only `pelican` is supported
DEFAULT_TAGS          | tags to be added output post
EMPTY_SUBJECT_TITLE   | title to be used if the original message has an empty subject



# License

I don’t believe in ‘intellectual property’. You can do whatever you want with the code, but please be kind and mention me as the original author if you redistribute it (something along the lines of CC-BY).



# Background information

You can read some background for this project [here](http://digitalwords.net/mail2staticsite-en/) (and [here](http://digitalwords.net/mail2staticsite/) in Hebrew).
