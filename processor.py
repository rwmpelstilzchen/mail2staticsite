import re
import datetime


def process_subject(subject, empty_subject_title):
    """Process the subject, making it suitable for the website.
    By defaults it removes common prefixes and if the result is an empty string,
    it substitutes it with EMPTY_SUBJECT_TITLE
    """
    prev_subject = ''
    while prev_subject != subject:
        prev_subject = subject
        subject = re.sub('((^re:)|(^fwd:)|(^fw:)|(^\s)|(\s$)|(^\[.*\]))*', '',
                         subject, flags=re.IGNORECASE)
    if subject == '':
        subject = empty_subject_title
    return subject


def process_body(body, markup_language):
    """Process the body, making it suitable for the website."""
    return re.sub('<img ', '<img onError="style.display = \'none\'" ', body)


def process_date(date):
    """Process the date as given in the message, outputting a list of this form:
        [%Y, %m, %d, %X, %z]"""
    strp_date = datetime.datetime.strptime(date, '%a, %d %b %Y %X %z')
    return strp_date.strftime("%Y %m %d %X %z").split()
