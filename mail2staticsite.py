#! /usr/bin/env python3

import argparse
import configparser
import mailbox
import email
import importlib
import os
import re
import sys


DEFAULT_CONFIG_NAME = 'config.cfg'
IMAGE_EXTS = ('jpg', 'jpeg', 'png')

config = []


def write_post_pelican_html(f, slug, title, body, date):
    global IMAGE_EXTS
    f.write("<html>")
    f.write("<head>")
    f.write("<title>" + title + "</title>")
    f.write("<meta name=\"slug\" content=\"" + slug + "\" />")
    f.write("<meta name=\"date\" content=\"" +
            date[0] + "-" + date[1] + "-" + date[2] + ' ' + date[3] +
            "\" />")
    f.write("</head>")
    f.write("<body>")
    f.write(body)
    for root, dirs, files in sorted(os.walk(os.path.join(config['MEDIA_DIR'],
                                                         slug))):
        images = [img for img in files if img.endswith(IMAGE_EXTS)]
        for image in sorted(images):
            # TODO: this is a special case, make it general!
            f.write("<a href=\"{filename}/images/posts/" +
                    slug + "/" + image + "\">\
                    <img src=\"{filename}/images/posts/" +
                    slug + "/" + image + "\" \
                    class=\"attachment\"/></a>")
        nonimages = sorted([x for x in files if x not in images])
        if nonimages:
            f.write("<ul>")
            for nonimage in sorted(nonimages):
                paperclip_glyph = '📎'
                f.write("<li><a href=\"{filename}/images/posts/" +
                        slug + "/" + nonimage + "\">" + paperclip_glyph + " " +
                        nonimage + "</a></li>")
            f.write("</ul>")
    f.write("</body>")
    f.write("</html>")


def write_post_pelican(f, slug, title, body, date, markup_language):
    if markup_language == 'html':
        write_post_pelican_html(f, slug, title, body, date)
    elif markup_language == 'md':
        write_post_pelican_md(f, slug, title, body, date)
    else:
        raise ValueError('unsupported markup language')


def write_post(slug, title, body, date, markup_language):
    global config
    filename = os.path.join(config['CONTENT_DIR'], slug +
                            '.' + markup_language)
    print("INFO: processing " + slug)
    if os.path.isfile(filename) and not config.getboolean('OVERWRITE'):
        return
    else:
        f = open(filename, 'w')
        if config['STATIC_SITE_GENERATOR'] == 'pelican':
            try:
                write_post_pelican(f, slug, title, body, date,
                                   markup_language)
            except ValueError:
                print("ERROR: Markup language '" + markup_language +
                      "' is unsupported for Pelican (" + filename + ").")
        else:
            print("ERROR: '" + config['STATIC_SITE_GENERATOR'] +
                  "' is not a supported static site generator")
            f.close()
            sys.exit()
        f.close()


def extract_body(message):
    if message.get_content_maintype() == 'multipart':
        for part in message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Type') == 'text/html; charset=UTF-8':
                return part.get_payload(decode=True).decode('UTF-8'), 'html'


def extract_attachments(message, slug):
    global config
    dirname = os.path.join(config['MEDIA_DIR'], slug)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    if message.get_content_maintype() == 'multipart':
        for part in message.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            raw_filename = part.get_filename()
            filename = re.sub('/', '', raw_filename)
            print("INFO: Saving attachment '" + filename +
                  "' (of " + slug + ").")
            fb = open(os.path.join(dirname, filename), 'wb')
            fb.write(part.get_payload(decode=True))
            fb.close()


def process_mailbox():
    global config
    maildir = mailbox.Maildir(config['MAIL_DIR'])
    processor = importlib.import_module(config['PROCESSOR'])
    for msg in maildir:
        # subject
        raw_subject, subject_enc = email.header.decode_header(msg['subject'])[0]
        if subject_enc is None:
            subject = processor.process_subject(raw_subject,
                                                config['EMPTY_SUBJECT_TITLE'])
        else:
            subject = processor.process_subject(raw_subject.decode(subject_enc),
                                                config['EMPTY_SUBJECT_TITLE'])
        # body
        raw_body, markup_language = extract_body(msg)
        body = processor.process_body(raw_body, markup_language)
        # date
        raw_date = email.header.decode_header(msg['date'])[0][0]
        date = processor.process_date(raw_date)
        # slug
        slug = date[0] + date[1] + date[2] +\
            re.sub(':', '', date[3]) + re.sub('\+', '', date[4])
        # attachments
        extract_attachments(msg, slug)
        write_post(slug, subject, body, date, markup_language)


def main():
    parser = argparse.ArgumentParser(
        description="Transform a local mail directory into content files,\
        to be processed with a static site generator.")
    parser.add_argument("-c", "--config", type=str, default=DEFAULT_CONFIG_NAME,
                        help="configuration file")
    # TODO: find a better name than ‘section’
    parser.add_argument("-s", "--section", type=str, default='DEFAULT',
                        help="section within configuration file")
    args = parser.parse_args()

    global config
    config_all = configparser.ConfigParser()
    config_all.read(args.config)
    config = config_all[args.section]

    process_mailbox()


if __name__ == "__main__":
    main()
