# Things to be done

## Urgent

* Generelization of media paths (the code right now is quite ugly): add configuration variable `MEDIA_INTERNAL_DIR`.
* [Markdown](http://daringfireball.net/projects/markdown/) and [reStructuredText](http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html) support for [Pelican](http://getpelican.com/).
* [Jekyll](https://jekyllrb.com/) support.
* Update documentation to reflect changes in the code (right now it is misleading).



## Not urgent

* Make use of `DEFAULT_TAGS`
* Add a working example (with maildir, a configuration file and a processor).
* Support for malformatted attachment filenames: if there is no extension, try to guess it according to MIME types or [`file`](https://en.wikipedia.org/wiki/File_(command)).
